# Panson

Panson is an interactive sonification framework based on sc3nb.

We also present a facial expression sonification Jupyeter Notebook based on OpenFace 2.0 and Panson.

## Installation

You can installed the required dependencies with the following command:

`pip install -r requirements.txt`

It is better to run it in a virtual environment. If you are using conda you can do:

`conda create --name panson python=3.9`
